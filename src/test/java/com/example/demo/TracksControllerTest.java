package com.example.demo;

import com.example.demo.controllers.TrackController;
import com.example.demo.domain.Track;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class TracksControllerTest {

    @Autowired
    private TrackController _trackController;

//    @Test
//    public void testGetById() {
//        Track track = _trackController.getById(3L).getBody();
//        assertNotNull(track);
//        assertEquals((long)track.getId(), 3L);
//    }

//    @Test
//    public void testDelete() {
//        _trackController.delete(2L);
//        Track track = _trackController.getById(2L).getBody();
//        assertNull(track);
//    }

//    @Test
//    public void testUpdate() {
//        Track track = _trackController.getById(3L).getBody();
//        assertNotNull(track);
//        String trackName = track.getName();
//        track.setName("Test name");
//        _trackController.update(track);
//        track = _trackController.getById(3L).getBody();
//        assertNotNull(track);
//        assertNotEquals(track.getName(), trackName);
//    }

//    @Test
//    public void testGet() {
//        String genre = "rock";
//        String author = "Bon Jovi";
//        String badGenre = "folk";
//        String badAuthor = "Mitar Miric";
//        List<Track> tracksByGenre = _trackController.get(Optional.of(genre),
//                Optional.empty(), Optional.empty(), Optional.empty()).getBody();
//        List<Track> tracksByAuthor = _trackController.get(Optional.empty(),
//                Optional.of(author), Optional.empty(), Optional.empty()).getBody();
//        List<Track> tracksByGenreAndAuthor = _trackController.get(Optional.of(genre),
//                Optional.of(author), Optional.empty(), Optional.empty()).getBody();
//        List<Track> tracksByGenreDoesntExist = _trackController.get(Optional.of(badGenre),
//                Optional.empty(), Optional.empty(), Optional.empty()).getBody();
//        List<Track> tracksByAuthorDoesntExist = _trackController.get(Optional.empty(),
//                Optional.of(badAuthor), Optional.empty(), Optional.empty()).getBody();
//        List<Track> tracksByGenreAndAuthorGenreDoesntExist = _trackController.get(Optional.of(badGenre),
//                Optional.of(author), Optional.empty(), Optional.empty()).getBody();
//        List<Track> tracksByGenreAndAuthorAuthorDoesntExist = _trackController.get(Optional.of(genre),
//                Optional.of(badAuthor), Optional.empty(), Optional.empty()).getBody();
//        List<Track> tracksByGenreAndAuthorBothDoesntExist = _trackController.get(Optional.of(badGenre),
//                Optional.of(badAuthor), Optional.empty(), Optional.empty()).getBody();
//
//        assertNotNull(tracksByGenre);
//        assertNotNull(tracksByAuthor);
//        assertNotNull(tracksByGenreAndAuthor);
//        assertNotNull(tracksByGenreDoesntExist);
//        assertNotNull(tracksByAuthorDoesntExist);
//        assertNotNull(tracksByGenreAndAuthorGenreDoesntExist);
//        assertNotNull(tracksByGenreAndAuthorAuthorDoesntExist);
//        assertNotNull(tracksByGenreAndAuthorBothDoesntExist);
//
//        for (Track track : tracksByGenre) {
//            assertEquals(track.getGenre().getName(), genre);
//        }
//        for (Track track : tracksByAuthor) {
//            assertEquals(track.getAuthor().getFullName(), author);
//        }
//        for (Track track : tracksByGenreAndAuthor) {
//            assertEquals(track.getGenre().getName(), genre);
//            assertEquals(track.getAuthor().getFullName(), author);
//        }
//
//        assertEquals(tracksByGenreDoesntExist.size(), 0);
//        assertEquals(tracksByAuthorDoesntExist.size(), 0);
//        assertEquals(tracksByGenreAndAuthorGenreDoesntExist.size(), 0);
//        assertEquals(tracksByGenreAndAuthorAuthorDoesntExist.size(), 0);
//        assertEquals(tracksByGenreAndAuthorBothDoesntExist.size(), 0);
//    }
}
