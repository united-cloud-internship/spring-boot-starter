package com.example.demo.services;

import com.example.demo.domain.Track;
import com.example.demo.utils.sorting.enums.SortingCategory;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface TracksService {
    public List<Track> sortTracks(List<Track> tracks,
                                  Optional<SortingCategory> sortingCategory,
                                  Optional<Sort.Direction> sortingOrder);

    public List<Track> filterTracks(Optional<String> genreName, Optional<String> authorName);
}
