package com.example.demo.services.implementation;

import com.example.demo.domain.Author;
import com.example.demo.domain.Genre;
import com.example.demo.domain.Track;
import com.example.demo.repository.AuthorRepository;
import com.example.demo.repository.GenreRepository;
import com.example.demo.repository.TrackRepository;
import com.example.demo.services.TracksService;
import com.example.demo.utils.sorting.enums.SortingCategory;
import com.example.demo.utils.sorting.TracksSortingComparator;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TracksServiceImpl implements TracksService {

    private final TrackRepository trackRepository;
    private final GenreRepository genreRepository;
    private final AuthorRepository authorRepository;

    public TracksServiceImpl(TrackRepository trackRepository,
                             GenreRepository genreRepository,
                             AuthorRepository authorRepository) {
        this.trackRepository = trackRepository;
        this.genreRepository = genreRepository;
        this.authorRepository = authorRepository;
    }
    @Override
    public List<Track> sortTracks(List<Track> tracks,
                                  Optional<SortingCategory> sortingCategory,
                                  Optional<Sort.Direction> sortingOrder) {
        if (!sortingCategory.isPresent() && !sortingOrder.isPresent()) {
            return tracks;
        } else if (sortingOrder.isPresent() && sortingCategory.isPresent()) {
            tracks.sort(new TracksSortingComparator(sortingCategory.get(), sortingOrder.get()));
            return tracks;
        } else if (sortingOrder.isPresent()) {
            tracks.sort(new TracksSortingComparator(SortingCategory.NAME, sortingOrder.get()));
            return tracks;
        } else {
            tracks.sort(new TracksSortingComparator(sortingCategory.get(), Sort.Direction.DESC));
            return tracks;
        }
    }

    @Override
    public List<Track> filterTracks(Optional<String> genreName, Optional<String> authorName) {
        Optional<Genre> genre = Optional.empty();
        Optional<Author> author = Optional.empty();

        if (!genreName.isPresent() && !authorName.isPresent()) {
            return trackRepository.findAll();
        }
        if (genreName.isPresent()) {
            genre = genreRepository.findByName(genreName.get());
        }
        if (authorName.isPresent()) {
            author = authorRepository.findByFullName(authorName.get());
        }
        if (genre.isPresent() && author.isPresent()) {
            return trackRepository.findAllByGenreAndAuthor(genre.get(), author.get());
        } else if (genre.isPresent() && !authorName.isPresent()) {
            return trackRepository.findAllByGenre(genre.get());
        } else if (author.isPresent() && !genreName.isPresent()) {
            return trackRepository.findAllByAuthor(author.get());
        } else {
            return new ArrayList<>();
        }
    }
}
