package com.example.demo.repository;

import com.example.demo.domain.Author;
import com.example.demo.domain.Genre;
import com.example.demo.domain.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TrackRepository extends JpaRepository<Track, Long> {
    List<Track> findAllByGenre(Genre genre);
    List<Track> findAllByGenreAndAuthor(Genre genre, Author author);
    List<Track> findAllByAuthor(Author author);
}
