package com.example.demo.utils.sorting.enums;

public enum SortingCategory {
    AUTHOR,
    GENRE,
    NAME
}
