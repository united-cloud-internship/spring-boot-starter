package com.example.demo.utils.sorting;

import com.example.demo.domain.Track;
import com.example.demo.utils.sorting.enums.SortingCategory;
import org.springframework.data.domain.Sort;

import java.util.Comparator;

public class TracksSortingComparator implements Comparator<Track> {

    private final SortingCategory sortingCategory;
    private final Sort.Direction sortingOrder;
    public TracksSortingComparator(SortingCategory sortingCategory, Sort.Direction sortingOrder) {
        this.sortingCategory = sortingCategory;
        this.sortingOrder = sortingOrder;
    }

    @Override
    public int compare(Track t1, Track t2) {
        switch (sortingCategory) {
            case AUTHOR:
                if (sortingOrder == Sort.Direction.ASC) {
                    return t1.getAuthor().getFullName().compareTo(t2.getAuthor().getFullName()) * (-1);
                } else if (sortingOrder == Sort.Direction.DESC) {
                    return t1.getAuthor().getFullName().compareTo(t2.getAuthor().getFullName());
                }
            case GENRE:
                if (sortingOrder == Sort.Direction.ASC) {
                    return t1.getGenre().getName().compareTo(t2.getGenre().getName()) * (-1);
                } else if (sortingOrder == Sort.Direction.DESC) {
                    return t1.getGenre().getName().compareTo(t2.getGenre().getName());
                }
            default:
                if (sortingOrder == Sort.Direction.ASC) {
                    return t1.getName().compareTo(t2.getName()) * (-1);
                } else if (sortingOrder == Sort.Direction.DESC) {
                    return t1.getName().compareTo(t2.getName());
                }
        }
        return 0;
    }
}
