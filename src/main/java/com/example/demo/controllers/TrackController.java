package com.example.demo.controllers;

import com.example.demo.domain.Track;
import com.example.demo.repository.TrackRepository;
import com.example.demo.services.TracksService;
import com.example.demo.utils.sorting.enums.SortingCategory;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/tracks")
public class TrackController {

    private final TrackRepository trackRepository;
    private final TracksService tracksService;

    public TrackController(TrackRepository trackRepository,
                           TracksService tracksService) {
        this.trackRepository = trackRepository;
        this.tracksService = tracksService;
    }

    @GetMapping("")
    public ResponseEntity<List<Track>> get(@RequestParam Optional<String> genreName,
                                           @RequestParam Optional<String> authorName,
                                           @RequestParam Optional<SortingCategory> sortingCategory,
                                           @RequestParam Optional<Sort.Direction> sortingOrder) {
        List<Track> tracks = tracksService.filterTracks(genreName, authorName);
        return new ResponseEntity<>(tracksService.sortTracks(tracks, sortingCategory, sortingOrder), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Track> getById(@PathVariable Long id) {
        Optional<Track> track = trackRepository.findById(id);
        if (!track.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(track.get(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<Track> post(@RequestBody Track track) {
        Track createdTrack = trackRepository.save(track);
        return new ResponseEntity<>(createdTrack, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Long id) {
        trackRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<Track> update(@RequestBody Track track) {
        Track updatedTrack = trackRepository.save(track);
        return new ResponseEntity<>(updatedTrack, HttpStatus.OK);
    }
}
