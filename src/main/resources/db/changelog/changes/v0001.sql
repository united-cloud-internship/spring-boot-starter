drop table if exists app_user;
drop table if exists tracks_in_playlist;
drop table if exists playlist;
drop table if exists track;
drop table if exists genre;
drop table if exists author;

create table app_user
(
    username varchar(50) primary key,
    password varchar(50) not null
);
create table genre
(
    id   serial primary key,
    name varchar(50) not null
);

create table author
(
    id        serial primary key,
    full_name varchar(100) not null
);

create table track
(
    id        serial primary key,
    name      varchar(50) not null,
    genre_id  int         not null,
    author_id int         not null,
    foreign key (genre_id) references genre (id),
    foreign key (author_id) references author (id)
);

create table playlist
(
    id   serial primary key,
    name varchar(50) not null
);

create table tracks_in_playlist
(
    id          serial primary key,
    track_id    serial not null,
    playlist_id serial not null,
    foreign key (track_id) references track (id),
    foreign key (playlist_id) references playlist (id)
);

insert into genre(name)
values ('rock');
insert into genre(name)
values ('rap');
insert into genre(name)
values ('pop');

insert into author(full_name)
values ('Ariana Grande');
insert into author(full_name)
values ('Bon Jovi');
insert into author(full_name)
values ('David Bowie');
insert into author(full_name)
values ('Dr Dre');
insert into author(full_name)
values ('Frank Sinatra');
insert into author(full_name)
values ('Post Malone');

insert into track(name, genre_id, author_id)
values ('Its my life', 3, 1);
insert into track(name, genre_id, author_id)
values ('Positions', 1, 2);
insert into track(name, genre_id, author_id)
values ('Blackstar', 3, 3);
insert into track(name, genre_id, author_id)
values ('Still D.R.E.', 2, 4);
insert into track(name, genre_id, author_id)
values ('My Way', 3, 5);
insert into track(name, genre_id, author_id)
values ('Circles', 3, 6);